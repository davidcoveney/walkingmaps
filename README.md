# WalkingMaps

Creative Commons Share-Alike 4.0 maps of towns in SVG format that show how to walk the area in the style of the London tube map.

# How to Edit

Each map is an SVG vector file that can be edited in the program Inkscape. You should make sure you have the latest version of Inkscape from inkscape.org allmaps have been created with Inkscape 1.0 (alpha2) and rendered to PNG for presentation.

# Contributing

You can clone this repository to make custom versions of the maps if you like. You can choose to contribute back your changes.

# Sources

Base layers are from the OpenStreetMaps project, although only used as a base. Other symbols and icons are from the public domain AIG set and other resources.
